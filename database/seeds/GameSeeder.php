<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GameSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("games")->insert([
            [
                'name' => 'HALO ONE',
                'category_id' => 1
            ],
            [
                'name' => 'HALO TWO',
                'category_id' => 2
            ],
            [
                'name' => 'Free Fire',
                'category_id' => 3
            ],
            [
                'name' => 'HungerGames',
                'category_id' => 4
            ],
            [
                'name' => 'GTA V',
                'category_id' => 5
            ]
            
        ]);
    }
}
