<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("users")->insert([
            [
                "email"         => "pepedroid26@gmail.com",
                "name"  => "Guadalupe Valerio",
                "password"      => Hash::make('123admin45.')
            ],
            [
                "email"         => "maria@gmail.com",
                "name"  => "Maria Alejo",
                "password"      => Hash::make('123admin45.')
            ],
            [
                "email"         => "robot@gmail.com",
                "name"  => "Dember",
                "password"      => Hash::make('123admin45.')
            ],

        ]);
    }
}
