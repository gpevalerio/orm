<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Game extends Model
{
 
    protected $filleable = ["name","category_id"];

    public function category(){
      return $this->belongsTo(Category::class);
    }

    /**
     * ASOCIAMOS N . M CON LA TABLA USER
     */

     public function users(){
         return $this->belongsToMany(User::class);
     }


}
