<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Category extends Model
{

    protected $filleable = ["name"]; // Insertamos las columnas de la tabla
    

    public function games(){
        return $this->hasMany(Game::class);
    }

}
